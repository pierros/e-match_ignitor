EESchema Schematic File Version 4
LIBS:ematch_ignitor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Concretedog E Match Ignitor"
Date ""
Rev "ver .003"
Comp ""
Comment1 "Designed to deliver 1 amp at 7.4 volts (2c Lipo) "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 5BB24B8F
P 6650 3400
F 0 "#PWR02" H 6650 3150 50  0001 C CNN
F 1 "GND" H 6655 3227 50  0000 C CNN
F 2 "" H 6650 3400 50  0001 C CNN
F 3 "" H 6650 3400 50  0001 C CNN
	1    6650 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BB24BB8
P 5050 3250
F 0 "C1" H 5165 3296 50  0001 L CNN
F 1 "470uf" H 5165 3250 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D8.0mm_H7.0mm_P3.50mm" H 5088 3100 50  0001 C CNN
F 3 "~" H 5050 3250 50  0001 C CNN
	1    5050 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BB24C83
P 5050 2800
F 0 "R1" H 5120 2846 50  0001 L CNN
F 1 "4.7r" H 5120 2800 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4980 2800 50  0001 C CNN
F 3 "~" H 5050 2800 50  0001 C CNN
	1    5050 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BB24CC6
P 7100 3000
F 0 "R2" H 7170 3046 50  0001 L CNN
F 1 "10k " H 7170 2955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7030 3000 50  0001 C CNN
F 3 "~" H 7100 3000 50  0001 C CNN
	1    7100 3000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q1
U 1 1 5BB24F12
P 6600 2700
F 0 "Q1" H 6806 2654 50  0001 L CNN
F 1 "IRF540N" H 6806 2700 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6850 2625 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf540n.pdf" H 6600 2700 50  0001 L CNN
	1    6600 2700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5BB25264
P 5850 2100
F 0 "J2" V 5697 2148 50  0001 L CNN
F 1 "Connector for Ematch" V 5900 2050 50  0000 C TNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5850 2100 50  0001 C CNN
F 3 "~" H 5850 2100 50  0001 C CNN
	1    5850 2100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5BB253C7
P 8000 2700
F 0 "J3" H 8106 2878 50  0001 C CNN
F 1 "GPIO Input" H 8106 2787 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 8000 2700 50  0001 C CNN
F 3 "~" H 8000 2700 50  0001 C CNN
	1    8000 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 2850 7100 2700
Wire Wire Line
	7100 2700 6800 2700
Wire Wire Line
	7100 3150 7100 3400
Wire Wire Line
	7100 3400 6900 3400
Wire Wire Line
	6500 2500 5700 2500
Wire Wire Line
	5700 2500 5700 3400
Wire Wire Line
	5700 3400 6650 3400
Connection ~ 6650 3400
Wire Wire Line
	5050 2550 5050 2600
Wire Wire Line
	5050 2950 5050 3000
Connection ~ 5700 3400
Wire Wire Line
	5050 3000 5350 3000
Connection ~ 5050 3000
Wire Wire Line
	5050 3000 5050 3100
Wire Wire Line
	5950 2300 5950 3000
Wire Wire Line
	5950 3000 6500 3000
Wire Wire Line
	6500 3000 6500 2900
Wire Wire Line
	5850 2300 5350 2300
Wire Wire Line
	5350 2300 5350 3000
$Comp
L power:VCC #PWR01
U 1 1 5BB26F8F
P 5050 2550
F 0 "#PWR01" H 5050 2400 50  0001 C CNN
F 1 "VCC" H 5067 2723 50  0000 C CNN
F 2 "" H 5050 2550 50  0001 C CNN
F 3 "" H 5050 2550 50  0001 C CNN
	1    5050 2550
	1    0    0    -1  
$EndComp
Text Label 5000 2300 0    50   ~ 0
7.4v
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5C334A11
P 4400 2700
F 0 "J1" H 4294 2375 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4294 2466 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 4400 2700 50  0001 C CNN
F 3 "~" H 4400 2700 50  0001 C CNN
	1    4400 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 2600 5050 2600
Connection ~ 5050 2600
Wire Wire Line
	5050 2600 5050 2650
Wire Wire Line
	4600 2700 4800 2700
Wire Wire Line
	4800 2700 4800 3400
Wire Wire Line
	4800 3400 5050 3400
Connection ~ 5050 3400
Wire Wire Line
	5050 3400 5650 3400
Wire Wire Line
	7800 2700 7100 2700
Connection ~ 7100 2700
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 5D83DD84
P 7100 1850
F 0 "J5" H 7127 1876 50  0000 L CNN
F 1 "Conn_01x01_Female" H 7127 1785 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 7100 1850 50  0001 C CNN
F 3 "~" H 7100 1850 50  0001 C CNN
	1    7100 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 5D83DDB2
P 7050 2050
F 0 "J4" H 7077 2076 50  0000 L CNN
F 1 "Conn_01x01_Female" H 7077 1985 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 7050 2050 50  0001 C CNN
F 3 "~" H 7050 2050 50  0001 C CNN
	1    7050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1850 6900 3400
Connection ~ 6900 3400
Wire Wire Line
	6900 3400 6850 3400
Wire Wire Line
	6850 2050 6850 3400
Connection ~ 6850 3400
Wire Wire Line
	6850 3400 6650 3400
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 5E74EC14
P 5850 3750
F 0 "J6" H 5877 3776 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5877 3685 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 5850 3750 50  0001 C CNN
F 3 "~" H 5850 3750 50  0001 C CNN
	1    5850 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3750 5650 3400
Connection ~ 5650 3400
Wire Wire Line
	5650 3400 5700 3400
$EndSCHEMATC
