This project is licensed under the CERN Open Hardware Licence Version 2 - permissive



**DISCLAIMER**, Use at your own risk, none of the authors, maintainers contributors or anyone else linked to this project can be held liable for any damage, loss or injury relating to the use of this design or derivative thereof. 

![Simple Schematic Image](/ematch_render.png)

This e-match ignitor circuit is designed to ignite the ubiquitous orange ematch ignitors that are often sold for remote firework ignition, but are also used commonly in dual deployment systems for rocketry. The circuit is primarily designed to be fed with a 2 cell lipo (7.4v) and this will deliver around 1.5 amps through the e-match when the GPIO “fire” pin is toggled high. You need to share the earth connection with whatever the circuit is that is going to toggle the GPIO, but it can be powered via a separate supply. In testing we have had no problems firing the circuit with a small (200mah) 7.4v 2 cell lipo powering both an Arduino and the e-match circuit, we have also tested the circuit with an Attiny based board powered off a separate 3.3volt source with the 7.4v lipo only supplying power to the ematch side of the circuit. 

We have had near 100 percent success rate igniting the orange e-matches, toggling the pin for as little as 5ms. 

![Simple Schematic Image](/orr_ematch_simple_circuit.png)

**Things of note! **

The PCB layout should be reasonably self explanatory and the BOM can be gathered from the NET list within the kicad files. Note that the standoff/mount holes are large pads connected to 
the ground plane flood and the pad in the upper right hand corner as viewed in the picture at the top of this document is a spare GND connection, useful if configuring 
the circuit with 2 power supplies. 
The circuit was designed to fire the orange ematches and may not supply enough power to light other types of igniter, whilst we haven’t characterised these, we read that estes igniters typically require 2 amps and Cesaroni e-match ignitors may require over 10 amps. Of course this circuit’s component values could be altered to attain these levels. Simple formula below where used to design this circuit. 

Having chosen a target current to be delivered (in our case 1.5 amps) we used Ohms law to work out what resistor we might need on the input section;

V=IR Where V is potential difference in volts, I is current in amps and R is resistance in ohms.

We rearranged this for for resistance R= V/I to give:

7.4/1.5=4.99993ohms

Rounding this to the nearest lower common resistor value this led us to choose a 4.7ohm resistor giving an actual current of 1.57446085amps.

We need to also consider the size of the capacitor and the charge time. A charge time of a capacitor is simply determined by the formula

t= 5RC where t is the time in seconds, R is the resistance in ohms and C is the capacitance in farads.   So with the 470uf capacitor in our circuit converted to Farads we get;

5*4.7*0.00047=0.011045 seconds. So this circuit is quite ready to fire after a little over a hundredth of a second. 

**Get in touch**

If you have any queries about this or other aspects of Open Research Rocketry projects or ideas get in touch via the website
[www.openresearchrocketry.co.uk](www.openresearchrocketry.co.uk)

